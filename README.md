TP 2 : Table View MBDS – M2
 
Objectives
L’objectif de ce TP est de vous permettre de réaliser une petite application comportant deux écrans : un écran présentant différentes titres sous forme de liste et un écran présentant le titre sélectionné.
Activités
1. Créer un projet de type Master-Detail Application
2. Préparer les données du Table View
a. Remplir le tableau self.object avec une série de titre (String) : self.object.addElement(« Titre 1 »)
3. Tester l'application
A quoi servent les méthodes suivantes ?
* NumberOfRowsInSection
* cellForRowAtIndexPath
* didSelectRowAtIndexPath
Ressources supplémentaires
* N/A
